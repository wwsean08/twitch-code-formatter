// add the appropriate css file
let highlightjsCss = $('<link>');
highlightjsCss.attr('rel', 'stylesheet');
highlightjsCss.attr('type', 'text/css');
highlightjsCss.attr('href', 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css');
$('head').append(highlightjsCss);

$(window).on('load', function(){
    hljs.initHighlightingOnLoad();
    $('.tw-pd-b-1').on('DOMNodeInserted', '.chat-line__message', function () {
        let messageElement = $(this).find(`[data-a-target='chat-message-text']`);
        if (messageElement.text().startsWith('```')) {
            let message = messageElement.text();
            let pre = $('<pre>');
            let code = $('<code>');
            message = message.replace('```', '');
            pre.append(code);
            code.text(message);
            messageElement.text('');
            messageElement.append(pre);
            messageElement.find('pre code').each(function (i, block) {
                hljs.highlightBlock(block);
            });
        }
    });
});
# twitch-code-formatter

Attempts to format and highlight source code that's sent in twitch chat.  This is based on https://github.com/clarkio/any-mask for reference and a discussion that took place on https://twitch.tv/clarkio.

# How this works

When a chat message starts with ``` (based on markdown) the plugin will take the text from the message and try to syntax highlight it.  Here are some examples:

![alt text](/image/example.png)

# Installation Instructions

1. Either download one of the artifact.zips and extract it's contents (including the zip folder it contains) from the build or download the repo.  
2. Then go to chrome://extensions and enable developer mode if it's not already enabled.
3. Select "Load Unpacked" and browse to the src folder from your download and hit ok, it should now be enabled.  You will need to restart your browser for it to take affect.